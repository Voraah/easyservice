package app.diego.com.easyservice;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    TextView txtProfile;
    Button btnLogOut;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtProfile = (TextView) findViewById(R.id.txtProfile);
        btnLogOut = (Button) findViewById(R.id.btnLogOut);
        if (AccessToken.getCurrentAccessToken()==null){
            goLoginActiviy();
        }
        Bundle extras = getIntent().getExtras();
        if (!extras.isEmpty()){
            txtProfile.setText(extras.getString("name"));

        }
        btnLogOut.setOnClickListener(this);
    }
    private void goLoginActiviy() {
        Intent inte = new Intent(this,LoginActivity.class);
        inte.addFlags(inte.FLAG_ACTIVITY_CLEAR_TOP|inte.FLAG_ACTIVITY_CLEAR_TASK|inte.FLAG_ACTIVITY_NEW_TASK);
        startActivity(inte);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogOut:
                Intent inte = new Intent(this,LoginActivity.class);
                inte.addFlags(inte.FLAG_ACTIVITY_CLEAR_TOP|inte.FLAG_ACTIVITY_CLEAR_TASK|inte.FLAG_ACTIVITY_NEW_TASK);
                startActivity(inte);
                LoginManager.getInstance().logOut();
                break;

        }
    }
}
